import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';

export interface IOrder {
  name: string;
}

@autoinject()
export class Orders {

  private orders: IOrder[];

  constructor(private router: Router) {
  }

  public attached() {
    this.orders = [{name: 'Jelo1'}, {name: 'Jelo2'}, {name: 'Jelo3'}];
  }

  public orderNow() {
    this.router.navigateToRoute('order-details');
  }
}
