import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';

@autoinject()
export class OrderDetails {

  constructor(private router: Router) {
  }


  public back() {
    this.router.navigateBack();
  }
}
