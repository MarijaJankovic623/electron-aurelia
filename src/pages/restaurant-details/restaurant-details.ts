import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';

@autoinject()
export class RestaurantDetails {
  constructor(private router: Router) {
  }

  public back() {
    this.router.navigateBack();
    ;
  }

}
