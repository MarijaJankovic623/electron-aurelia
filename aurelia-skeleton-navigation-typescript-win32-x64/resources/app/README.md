Za pokretanje ovog programa potrebno je 

1. Imati instaliran nodeJS http://nodejs.org/ i nakon toga pokrenuti **npm install**

2. Instalirati Gulp http://gulpjs.com/ globalno
npm install -g gulp

3. Imati instaliran jspm http://jspm.io/ globalno,
moguce ga je instalirati pomocu komande **npm install -g jspm**
i nakon toga pokrenuti **jspm install**

4. electron http://electron.atom.io 
 pomocu npm-a 
 npm install electron --save-dev


Da biste aplikaciju pokrenuli kao veb aplikaciju:
**gulp watch**

i aplikacija ce podrazumevano biti pokrenula na *http://localhost:9000*

Da biste izvrsili build aplikacije
**gulp build**

Da biste pokrenuli aplikaciju u elektronu kao desktop aplikaciju:
**electron index.js**
