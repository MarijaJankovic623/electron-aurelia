import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';

export interface IDish {
  name: string;
}

@autoinject()
export class RestaurantMenu {
  private dishes: IDish[];

  constructor(private router: Router) {
  }

  public attached() {
    this.dishes = [{name: 'Lazanje'}, {name: 'Cezar salata'}, {name: 'Cokoladna torta'}];
  }

  public leaveComment(){
    this.router.navigateToRoute('comment');
  }

  public back() {
    this.router.navigateToRoute('restaurants-list');
  }
}
