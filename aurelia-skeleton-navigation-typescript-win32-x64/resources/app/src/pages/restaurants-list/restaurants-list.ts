import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

export interface IRestaurant {
  name: string;
}

@autoinject()
export class RestaurantsList {
  private restaurants: IRestaurant[];

  constructor(private router: Router) {
  }

  public attached() {
    this.restaurants = [{name: 'Kosuta'}, {name: 'Kisobran'}, {name: 'Bajka'}];
  }

  public viewDetails(restaurant: IRestaurant) {
    this.router.navigateToRoute('details');
  }

  public viewMenu(restaurant: IRestaurant) {
    this.router.navigateToRoute('menu');
  }


  public leaveComment(){
    this.router.navigateToRoute('comment');
  }

}
